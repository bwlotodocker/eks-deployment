FROM ubuntu:18.04

ENV TF_CLI_CONFIG_FILE /terraform.rc
ENV DEBIAN_FRONTEND noninteractive
ENV HELM_VERSION="v3.3.1"
ENV HELMFILE_VERSION="v0.135.0"
ENV TERRAFORM_VERSION="1.3.2"

RUN apt-get update
RUN ln -fs /usr/share/zoneinfo/Iceland /etc/localtime
RUN apt-get install -y tzdata
RUN dpkg-reconfigure --frontend noninteractive tzdata

RUN apt-get install -y wget
RUN apt-get install -y curl
RUN apt-get install -y unzip
RUN apt-get install -y nodejs
RUN apt-get install -y npm
RUN apt-get install -y awscli
RUN apt-get install -y python3-pip
RUN apt-get install -y jq
RUN pip3 install awscli --upgrade --user
RUN wget https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip
RUN unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip
RUN mv terraform /usr/local/bin
RUN rm -f terraform_${TERRAFORM_VERSION}_linux_amd64.zip
RUN curl -L https://dl.k8s.io/release/v1.18.2/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl
RUN chmod +x /usr/local/bin/kubectl

RUN wget -q https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz -O - | tar -xzO linux-amd64/helm > /usr/local/bin/helm
RUN chmod +x /usr/local/bin/helm

ADD https://github.com/roboll/helmfile/releases/download/${HELMFILE_VERSION}/helmfile_linux_amd64 /usr/local/bin/helmfile
RUN chmod +x /usr/local/bin/helmfile
RUN helmfile version

RUN curl -L https://istio.io/downloadIstio | ISTIO_VERSION=1.17.1 sh -
RUN mv istio-1.17.1/bin/istioctl /usr/local/bin
RUN chmod +x /usr/local/bin/istioctl

RUN apt-get -y install git
RUN git --version

RUN helm plugin install https://github.com/databus23/helm-diff
